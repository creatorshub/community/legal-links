//<?php

$form->add( new \IPS\Helpers\Form\Text( 'legallink_imprint', \IPS\Settings::i()->legallink_imprint ) );
$form->add( new \IPS\Helpers\Form\Text( 'legallink_privacy', \IPS\Settings::i()->legallink_privacy ) );
$form->add( new \IPS\Helpers\Form\Text( 'legallink_terms', \IPS\Settings::i()->legallink_terms ) );

if ( $values = $form->values() )
{
	$form->saveAsSettings();
	return TRUE;
}

return $form;