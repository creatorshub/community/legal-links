//<?php

/* To prevent PHP errors (extending class does not exist) revealing path */
if ( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
	exit;
}

class hook20 extends _HOOK_CLASS_
{

/* !Hook Data - DO NOT REMOVE */
public static function hookData() {
 return array_merge_recursive( array (
  'footer' => 
  array (
    0 => 
    array (
      'selector' => '#elFooterLinks',
      'type' => 'add_inside_end',
      'content' => '{{if $imprint=\IPS\Settings::i()->legallink_imprint}}
<li><a href="{$imprint}">{lang="legallink_imprint"}</a></li>
{{endif}}
{{if $privacy=\IPS\Settings::i()->legallink_privacy}}
<li><a href="{$privacy}">{lang="legallink_privac"}</a></li>
{{endif}}
{{if $terms=\IPS\Settings::i()->legallink_terms}}
<li><a href="{$terms}">{lang="legallink_terms"}</a></li>
{{endif}}',
    ),
  ),
), parent::hookData() );
}
/* End Hook Data */


}
